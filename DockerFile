FROM java:8
EXPOSE 9099
ADD /target/zuulOrchestration.jar zuulOrchestration.jar
ENTRYPOINT ["java", "-jar", "zuulOrchestration.jar"]